.PHONY: deps clean

deps:
	pip install -r requirements.txt

clean:
	find -iname "*.pyc" -delete
	find -iname "*.swp" -delete

test: clean
	nosetests -v --rednose --with-randomly
