import random


class Traveller(object):

    def __init__(self, number):
        self.number = number
        self.steps = 0
        self.station = None
        self.is_blocked = False
        self.is_happy = False

    def travel(self):
        destination = None
        if not self.is_blocked:
            destination = self._get_destination()

        if destination:
            self._update(destination)
            self.steps += 1
            return True
        else:
            self.is_blocked = True
            return False

    def _get_destination(self):
        destinations = self.station.get_open_connections()
        return self._get_random_selection(destinations)

    def _update(self, destination):
        self.station.leave(self.number)
        self.station = destination
        self.station.arrive(self.number)

    def _get_random_selection(self, destination_list):
        # if we have somewhere to go
        if destination_list:
            destination_index = random.randint(0, len(destination_list) - 1)
            return destination_list[destination_index]


class Cat(Traveller):
    def __init__(self, number):
        super(Cat, self).__init__(number)


class Owner(Traveller):

    def __init__(self, number):
        super(Owner, self).__init__(number)
        self.visited_places = list()

    def _get_destination(self):
        # We are smarter, we take note of where we are
        self.visited_places.append(self.station.number)

        destinations = self.station.get_open_connections()

        # see if we have a new place to go
        new_destinations = list(set(destinations) - set(self.visited_places))
        if new_destinations:
            destinations = new_destinations

        return self._get_random_selection(destinations)

    def cat_found(self):
        return self.station.is_my_cat_here(self.number)
