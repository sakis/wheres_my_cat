import json
import os
import random


class TubeMap(object):
    STATIONS_FILE = os.path.join("data", "tfl_stations.json")
    CONNECTIONS_FILE = os.path.join("data", "tfl_connections.json")

    def __init__(self):
        self.stations = dict()
        self._get_stations()
        self._add_connections()

    def _read_json(self, path):
        with open(path, "r") as fp:
            return json.load(fp)

    def _get_stations(self):
        json_list = self._read_json(self.STATIONS_FILE)
        for pair in json_list:
            # Assuming that each station id appears exactly one time
            # and the ids are only numeric
            self.stations[int(pair[0])] = Station(pair)

    def _add_connections(self):
        json_list = self._read_json(self.CONNECTIONS_FILE)
        for pair in json_list:
            station_a = self.stations[int(pair[0])]
            station_b = self.stations[int(pair[1])]

            station_a.add_connection(station_b)
            station_b.add_connection(station_a)

    def get_random_stations(self):
        r1 = random.choice(self.stations.values())
        r2 = random.choice(self.stations.values())
        while r2 == r1:
            r2 = random.choice(self.stations.values())

        return (r1, r2)

    def get_station(self, number):
        return self.stations[number]


class Station(object):

    def __init__(self, station_data):
        self.number = int(station_data[0])
        self.name = station_data[1]
        self.connections = set()
        self.is_open = True
        self.travellers = list()

    def add_connection(self, station):
        self.connections.add(station)

    def get_open_connections(self):
        return [station for station in self.connections if station.is_open]

    def leave(self, number):
        self.travellers.remove(number)

    def arrive(self, number):
        self.travellers.append(number)

    def is_my_cat_here(self, number):
        if self.travellers.count(number) > 1:
            self.is_open = False

            # Both the cat and the human leave
            self.leave(number)
            self.leave(number)

        return not self.is_open
