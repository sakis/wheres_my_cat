import random

from travellers import Cat, Owner


class Game(object):

    def __init__(self, tube_map, count, max_rounds=100000):
        self.count = int(count)
        self.cats = list()
        self.owners = list()
        self.total_steps = list()
        self.found = 0
        self.max_rounds = max_rounds
        self.tube_map = tube_map

        random.seed()
        self._randomise_travellers()

    def _randomise_travellers(self):
        for i in xrange(self.count):
            cat = Cat(i)
            owner = Owner(i)

            cat.station, owner.station = self.tube_map.get_random_stations()

            self.tube_map.get_station(cat.station.number).arrive(i)
            self.tube_map.get_station(owner.station.number).arrive(i)

            self.cats.append(cat)
            self.owners.append(owner)

    def _make_travel(self, traveller):
        if not traveller.is_blocked and not traveller.is_happy:
            traveller.travel()

    def _is_everyone_blocked(self):
        # we don't care about cats and owners who found each other
        # as they don't move
        cats_blocked = len(
            [cat for cat in self.cats if cat.is_blocked and not cat.is_happy])
        owners_blocked = len(
            [owner for owner in self.owners if owner.is_blocked and not owner.is_happy])  # noqa

        return cats_blocked + owners_blocked >= 2 * (self.count - self.found)

    def _look_for_the_cats(self, index):
        cat = self.cats[index]
        owner = self.owners[index]

        # if that owner doesn't have the cat he should look for it
        if not owner.is_happy and owner.cat_found():
            station_name = owner.station.name
            print("Owner {} found cat {} - {} is now closed".format(
                  index, index, station_name))
            self.found += 1
            self.total_steps.append(owner.steps)
            owner.is_happy = True
            cat.is_happy = True

    def _show_results(self):
        print("Total number of cats: {}".format(self.count))
        print("Number of cats found: {}".format(self.found))
        print("Average number of movements required "
              "to find a cat: {}".format(round(
                  sum(self.total_steps) / len(self.total_steps)))
              )

    def play(self):
        for i in xrange(self.max_rounds):

            # first we move everyone
            for j in xrange(self.count):
                self._make_travel(self.cats[j])
                self._make_travel(self.owners[j])

            # then we look for the cats
            for j in xrange(self.count):
                self._look_for_the_cats(j)

            # and finally check if we can stop
            if self.found == self.count or self._is_everyone_blocked():
                break

        self._show_results()
