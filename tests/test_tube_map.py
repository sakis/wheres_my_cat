import unittest

from mock import patch

from tfl import TubeMap


@patch("tfl.json")
@patch("__builtin__.open")
class TestTubeMap(unittest.TestCase):

    STATIONS = [
        ["1", "Acton Town"],
        ["2", "Aldgate"],
        ["3", "Aldgate East"],
        ["4", "All Saints"],
        ["5", "Alperton"]
    ]

    CONNECTIONS = [
        ["1", "3"],
        ["1", "2"],
        ["1", "5"],
        ["2", "4"],
        ["3", "4"],
        ["5", "3"],
        ["2", "3"]
    ]

    def test_correct_station_creation(self, m_open, m_json):
        m_json.load.side_effect = [self.STATIONS, self.CONNECTIONS]
        tube_map = TubeMap()

        self.assertEqual(5, len(tube_map.stations))

        self.assertEqual(3, len(tube_map.stations[1].connections))
        self.assertEqual(3, len(tube_map.stations[2].connections))
        self.assertEqual(4, len(tube_map.stations[3].connections))
        self.assertEqual(2, len(tube_map.stations[4].connections))
        self.assertEqual(2, len(tube_map.stations[5].connections))

    def test_get_station(self, m_open, m_json):
        m_json.load.side_effect = [self.STATIONS, self.CONNECTIONS]
        tube_map = TubeMap()

        station = tube_map.get_station(1)
        self.assertEqual(station.number, 1)
