import unittest

from mock import Mock, patch

from tfl import Station


class TestStation(unittest.TestCase):

    def test_station_initialization(self):
        st = Station(("3", "Test Station"))

        self.assertEqual(st.number, 3)
        self.assertEqual(st.name, "Test Station")
        self.assertEqual(len(st.connections), 0)
        self.assertEqual(len(st.travellers), 0)
        self.assertEqual(st.is_open, True)

    def test_station_add_connection(self):
        st = Station(("3", "Test Station"))

        mock_station = Mock()
        st.add_connection(mock_station)
        self.assertEqual(len(st.connections), 1)
        st.add_connection(mock_station)
        self.assertEqual(len(st.connections), 1)
        st.add_connection(Mock())
        self.assertEqual(len(st.connections), 2)

    def test_station_get_open_connections(self):
        mock_open_1 = Mock(is_open=True)
        mock_open_2 = Mock(is_open=True)
        mock_closed_1 = Mock(is_open=False)

        st = Station(("3", "Test Station"))
        st.add_connection(mock_open_1)
        st.add_connection(mock_open_2)
        st.add_connection(mock_closed_1)

        open_connections = st.get_open_connections()
        self.assertEqual(len(open_connections), 2)
        self.assertTrue(mock_open_1 in open_connections)
        self.assertTrue(mock_open_2 in open_connections)
        self.assertTrue(mock_closed_1 not in open_connections)

    def test_station_leave_single_occurence(self):
        st = Station(("3", "Test Station"))
        st.travellers = [1, 2, 3, 4]

        st.leave(2)
        self.assertEqual(len(st.travellers), 3)

    def test_station_leave_multiple_occurences(self):
        st = Station(("3", "Test Station"))
        st.travellers = [1, 2, 3, 4, 2]

        st.leave(2)
        self.assertEqual(len(st.travellers), 4)

    def test_station_arrive(self):
        st = Station(("3", "Test Station"))

        st.arrive(1)
        st.arrive(2)
        st.arrive(3)
        st.arrive(4)
        self.assertEqual(len(st.travellers), 4)

    @patch.object(Station, "leave")
    def test_is_my_cat_here_true(self, m_leave):
        st = Station(("3", "Test Station"))
        st.travellers = [1, 2, 3, 4, 2]

        ret = st.is_my_cat_here(2)
        self.assertEqual(m_leave.call_count, 2)
        self.assertFalse(st.is_open)
        self.assertTrue(ret)

    @patch.object(Station, "leave")
    def test_is_my_cat_here_false(self, m_leave):
        st = Station(("3", "Test Station"))
        st.travellers = [1, 2, 3, 4, 2]

        ret = st.is_my_cat_here(3)
        self.assertEqual(m_leave.call_count, 0)
        self.assertFalse(ret)
        self.assertTrue(st.is_open)
