# Where is my cat

An equal number of cats and their owners are scattered around the Tube.
The owners are looking for their cats while the cats are just... being cats :)

## How to run it

Just run `python where_is_my_cat.py X` where X is how many cats and owners you want to
play with.

If you want to run the tests, create a virtualevn, activate it and run
`make deps; make test`

## Assumptions

The following assumptions were made:

1. The connections between the stations are bidirectional
1. Only the owners' movements are measured to get the average
1. The data are correct (eg. there's no check if the station ids are not numbers)

## Logic

The program works as follows:

1. Read the data and create the stations and their connections
1. Create N cats and N owners, where N is given as a command line parameter
1. Place the cats and owners randomly on the stations, taking care not to put them together
1. Loop through all the cats and the owners and move them to a (randomly chosen) connected station
1. Loop through all the owners and check if they found their cat

The program ends when either all the cats are found or the max number of rounds has been reached,
or if everyone is blocked and they cannot move

## How the code is organized

There are 6 classes:

1. TubeMap (in tfl.py) which initializes and maintains a dict with all the stations
1. Station (in tfl.py) which contains the relevant data for each station, ie. information
like the id of the station, which stations are they connected with, if it is open or closed
and also maintains a list of the Travellers (cats or owners) that are in the station at the moment
1. Traveller (in travellers.py) a base class for Owner and Cat which contains much of the shared
logic about travelling between the stations
1. Cat (in travellers.py) a subclass of Traveller which only exists for semantic puproses
1. Owner (in travellers.py) a subclass of Traveller which overrides some of the travel logic and
also maintains a list of the visited destinations
1. Game (in game.py) which is used to implement the basic steps of the game. Initializes cats and owners to their
initial stations, makes them travel around on each round and makes the owners check if their cat is found
