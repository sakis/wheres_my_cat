import sys

from game import Game
from tfl import TubeMap

MAX_ROUNDS = 100000


def main(count):
    tube_map = TubeMap()

    game = Game(tube_map, count, MAX_ROUNDS)
    game.play()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("Please provide the number of cats and owners")

    main(sys.argv[-1])
